package com.givaldoms.dvdinfinity.data.repository

import com.givaldoms.dvdinfinity.core.DataRows
import com.givaldoms.dvdinfinity.data.repository.remote.model.UserResponse
import com.givaldoms.dvdinfinity.model.Midia
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.*

interface WebService {

    @GET(PATH_GET_MEDIAS)
    fun getMidias(
            @Header(PARAM_TOKEN) token: String
    ): Deferred<DataRows<Midia>>


    @GET(PATH_GET_USER_BY_ID)
    fun getUser(
            @Header(PARAM_TOKEN) token: String,
            @Path(PARAM_USER_ID) userId: String
    ): Deferred<UserResponse>

    @POST(PATH_POST_USER)
    fun saveForm(
            @Header(PARAM_TOKEN) token: String,
            @Body user: UserResponse
    ): Deferred<UserResponse>

    @PATCH(PATH_PUSH_ID)
    fun sendMessageToken(
            @Header(PARAM_TOKEN) token: String,
            @Path(PARAM_USER_ID) userId: String,
            @Body pushId: HashMap<String, String>
    ): Deferred<HashMap<String, Any>>


    companion object {
        private const val BASE_URL = ""

        private const val PARAM_TOKEN = "Authorization"
        private const val PARAM_USER_ID = "userId"

        private const val PATH_GET_MEDIAS = "midias"
        private const val PATH_GET_USER_BY_ID = "users/{$PARAM_USER_ID}"
        private const val PATH_POST_USER = "users"
        private const val PATH_PUSH_ID = "users/{$PARAM_USER_ID}/pushId"

    }

}