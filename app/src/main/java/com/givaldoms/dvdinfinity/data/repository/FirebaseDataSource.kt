package com.givaldoms.dvdinfinity.data.repository

import com.givaldoms.dvdinfinity.core.AuthException
import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.core.asDeferred
import com.givaldoms.dvdinfinity.core.safeApiCall
import com.google.firebase.auth.FirebaseAuth

interface FirebaseDataSource {

    suspend fun firebaseLogin(email: String, password: String): Result<Boolean>

    suspend fun firebaseSignUp(email: String, password: String): Result<Boolean>

    suspend fun firebaseRecoverPassword(email: String): Result<Boolean>

    suspend fun getToken(): Result<String>

    suspend fun getUserId(): Result<String>

    suspend fun logout()

}

class FirebaseDataSourceImpl(
        private val auth: FirebaseAuth
): FirebaseDataSource {

    override suspend fun firebaseLogin(email: String, password: String): Result<Boolean> {
        return safeApiCall { auth.signInWithEmailAndPassword(email, password).asDeferred().await() != null }
    }

    override suspend fun firebaseSignUp(email: String, password: String): Result<Boolean> {
        return safeApiCall { auth.createUserWithEmailAndPassword(email, password).asDeferred().await() != null }
    }

    override suspend fun getToken(): Result<String> {
        val tokenResult = safeApiCall { auth.currentUser?.getIdToken(true)?.asDeferred()?.await() }

        return when (tokenResult) {
            is Result.Success -> {
                when {
                    tokenResult.data?.token != null -> {
                        val t = tokenResult.data.token
                        if (t == null) Result.Error(AuthException())
                        else Result.Success(t)
                    }
                    else -> Result.Error(AuthException())
                }
            }

            is Result.Error -> Result.Error(AuthException())
        }
    }

    override suspend fun firebaseRecoverPassword(email: String): Result<Boolean> {
        return safeApiCall { auth.sendPasswordResetEmail(email).asDeferred().await() != null }
    }

    override suspend fun getUserId(): Result<String> {
        val uid = auth.currentUser?.uid

        return when (uid) {
            null -> Result.Error(AuthException())
            else -> Result.Success(uid)
        }
    }

    override suspend fun logout() {
        auth.signOut()
    }
}