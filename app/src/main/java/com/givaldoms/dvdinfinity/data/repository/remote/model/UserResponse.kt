package com.givaldoms.dvdinfinity.data.repository.remote.model

class UserResponse(
    val nome: String?,
    val rg: String?,
    val orgEmissor: String?,
    val whatsapp: String?,
    val instagran: String?,
    val sexo: String?,
    val dataNascimento: String?,
    val cidade: String?,
    val bairro: String?,
    val pushId: String?,
    val indicacao: String?,
    val sorteado: Boolean?,
    val uid: String?
)
