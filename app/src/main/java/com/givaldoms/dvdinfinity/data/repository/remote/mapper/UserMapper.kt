package com.givaldoms.dvdinfinity.data.repository.remote.mapper

import com.givaldoms.dvdinfinity.core.extenstions.formatToDate
import com.givaldoms.dvdinfinity.data.repository.remote.model.UserResponse
import com.givaldoms.dvdinfinity.model.User

object UserMapper : Mapper<UserResponse, User> {

    override fun parse(model: UserResponse) =
        User(
                model.uid ?: "",
                model.nome ?: "",
                model.rg ?: "",
                model.orgEmissor ?: "",
                model.whatsapp ?: "",
                model.instagran ?: "",
                model.sexo ?: "",
                model.dataNascimento ?: "",
                model.cidade ?: "",
                model.bairro ?: "",
                model.pushId ?: "",
                model.indicacao ?: "",
                model.sorteado ?: false
        )

    override fun toRemote(domain: User) =
        UserResponse(
                nome = domain.nome,
                rg = domain.rg,
                orgEmissor = domain.orgEmissor,
                whatsapp = domain.whatsapp,
                instagran = domain.instagram,
                sexo = domain.sexo,
                dataNascimento = domain.dataNascimento.formatToDate(),
                cidade = domain.cidade,
                bairro = if (domain.bairro.isBlank()) "outro" else domain.bairro,
                pushId = domain.pushId,
                indicacao = domain.indicacao,
                sorteado = null, uid = null)

}

