package com.givaldoms.dvdinfinity.data.repository

import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.core.safeApiCall
import com.givaldoms.dvdinfinity.data.repository.remote.mapper.UserMapper
import com.givaldoms.dvdinfinity.model.Midia
import com.givaldoms.dvdinfinity.model.User

interface RemoteRepository {
    suspend fun sendForm(token: String, user: User): Result<User>
    suspend fun getUser(token: String, userId: String): Result<User>
    suspend fun getMedias(token: String): Result<List<Midia>>
    suspend fun sendPushId(token: String, userId: String, pushId: String): Result<String>

}

class RemoteRepositoryImpl(private val webService: WebService)
    : RemoteRepository {

    override suspend fun sendForm(token: String, user: User): Result<User> {
        return safeApiCall {
            UserMapper.parse(webService.saveForm(token, UserMapper.toRemote(user)).await())
        }
    }

    override suspend fun getUser(token: String, userId: String): Result<User> {
        return safeApiCall { UserMapper.parse(webService.getUser(token, userId).await()).copy() }
//        return Result.Error(RemoteDataNotFoundException())
    }

    override suspend fun getMedias(token: String): Result<List<Midia>> {
        return safeApiCall { webService.getMidias(token).await().data.rows }
    }

    override suspend fun sendPushId(token: String, userId: String, pushId: String): Result<String> {
        safeApiCall { webService.sendMessageToken(token, userId, hashMapOf("pushId" to pushId)).await() }
        return Result.Success(pushId)
    }
}