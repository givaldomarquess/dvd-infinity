package com.givaldoms.dvdinfinity.data.repository.remote.mapper

interface Mapper<M, D> {
    fun parse(model: M): D

    fun toRemote(domain: D): M

}