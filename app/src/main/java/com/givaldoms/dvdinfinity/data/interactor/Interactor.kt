package com.givaldoms.dvdinfinity.data.interactor

import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.data.repository.FirebaseDataSource
import com.givaldoms.dvdinfinity.data.repository.RemoteRepository
import com.givaldoms.dvdinfinity.model.Midia
import com.givaldoms.dvdinfinity.model.User

interface Interactor {
    suspend fun login(email: String, password: String): Result<Boolean>
    suspend fun signUp(email: String, password: String): Result<Boolean>
    suspend fun recoverPassword(email: String): Result<Boolean>
    suspend fun getUser(): Result<User>
    suspend fun sendForm(user: User): Result<User>
    suspend fun getImages(): Result<List<Midia>>
    suspend fun sendMessageToken(messageToken: String): Result<String>

}

class InteractorImpl(private val remote: RemoteRepository,
                     private val firebase: FirebaseDataSource) : Interactor {

    override suspend fun login(email: String, password: String): Result<Boolean> {
        return firebase.firebaseLogin(email, password)
    }

    override suspend fun signUp(email: String, password: String): Result<Boolean> {
        return firebase.firebaseSignUp(email, password)
    }

    override suspend fun recoverPassword(email: String): Result<Boolean> {
        return firebase.firebaseRecoverPassword(email)
    }

    override suspend fun getUser(): Result<User> {
        val tokenResult = firebase.getToken()
        val userId = firebase.getUserId()

        return when {
            tokenResult is Result.Success && userId is Result.Success -> {
                remote.getUser(tokenResult.data, userId.data)
            }
            tokenResult is Result.Error -> tokenResult
            userId is Result.Error -> userId
            else -> Result.Error(Exception())

        }
    }

    override suspend fun sendForm(user: User): Result<User> {
        val tokenResult = firebase.getToken()
        val userId = firebase.getUserId()

        return when {
            tokenResult is Result.Success && userId is Result.Success -> {
                remote.sendForm(tokenResult.data, user)
            }

            tokenResult is Result.Error -> tokenResult
            userId is Result.Error -> userId
            else -> Result.Error(Exception())
        }
    }

    override suspend fun getImages(): Result<List<Midia>> {
        val tokenResult = firebase.getToken()

        return when (tokenResult) {
            is Result.Error -> tokenResult
            is Result.Success -> remote.getMedias(tokenResult.data)
        }
    }

    override suspend fun sendMessageToken(messageToken: String): Result<String> {
        val tokenResult = firebase.getToken()
        val userId = firebase.getUserId()

        return when {
            tokenResult is Result.Success && userId is Result.Success -> {
                remote.sendPushId(tokenResult.data, userId.data, messageToken)
            }

            tokenResult is Result.Error -> tokenResult
            userId is Result.Error -> userId
            else -> Result.Error(Exception())
        }

    }
}




