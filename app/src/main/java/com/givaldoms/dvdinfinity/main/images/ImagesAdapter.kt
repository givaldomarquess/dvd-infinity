package com.givaldoms.dvdinfinity.main.images

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.core.OnItemClickListener
import com.givaldoms.dvdinfinity.model.Midia
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_image.view.*

class ImagesAdapter(private val items: List<Midia>,
                    private val listener: OnItemClickListener<Midia>) : RecyclerView.Adapter<ImagesAdapter.ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_image, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(items[position].url) {
            listener.onItemClick(position, items[position])
        }
    }


    class ImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val image: ImageView? = itemView.imageView
        private val shareButton: Button? = itemView.shareButton

        fun bind(item: String, shareClick:() -> Unit) {

            image?.let {
                Picasso.get()
                        .load(item)
                        .into(it)
            }

            shareButton?.setOnClickListener {
                shareClick()
            }
        }
    }

}
