package com.givaldoms.dvdinfinity.main.images


import android.Manifest
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.core.OnItemClickListener
import com.givaldoms.dvdinfinity.core.extenstions.findView
import com.givaldoms.dvdinfinity.core.extenstions.shareUri
import com.givaldoms.dvdinfinity.model.Midia
import kotlinx.android.synthetic.main.fragment_images.*
import org.koin.android.viewmodel.ext.android.viewModel
import pub.devrel.easypermissions.EasyPermissions

class ImagesFragment : Fragment(), OnItemClickListener<Midia> {

    companion object {
        fun newInstance() = ImagesFragment()
    }

    private val viewModel: ImagesViewModel by viewModel()
    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_images, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lifecycle.addObserver(viewModel)

        viewModel.viewState.observe(this, Observer {
            setupRecyclerView(it.data ?: emptyList())
        })
    }

    private fun setupRecyclerView(items: List<Midia>) {
        val adapter = ImagesAdapter(items, this)
        imagesRecyclerView.layoutManager = layoutManager
        imagesRecyclerView.adapter = adapter


    }

    override fun onItemClick(position: Int, item: Midia) {
        share(position, item.mensagem)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)

    }

    private fun share(postion: Int, text: String) {
        val perms = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val ctx = context ?: return
        if (EasyPermissions.hasPermissions(ctx, perms)) {

            try {
                val imgView1: ImageView = layoutManager.findView(postion,
                        R.id.imageView) ?: return

                val bitmap1 = (imgView1.drawable as BitmapDrawable).bitmap

                ctx.shareUri(text, bitmap1)

            } catch (ex: TypeCastException) {
                Toast.makeText(ctx, "Aguarde a imagem carregar", Toast.LENGTH_SHORT).show()
            }


        } else {
            EasyPermissions.requestPermissions(this, "Conceda a permissão",
                    0, perms)
        }
    }


}
