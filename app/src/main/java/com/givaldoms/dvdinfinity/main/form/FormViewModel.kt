package com.givaldoms.dvdinfinity.main.form

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.core.AuthException
import com.givaldoms.dvdinfinity.core.CollisionException
import com.givaldoms.dvdinfinity.core.RemoteDataNotFoundException
import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.core.extenstions.MaskWatcher
import com.givaldoms.dvdinfinity.data.interactor.Interactor
import com.givaldoms.dvdinfinity.model.User
import com.givaldoms.dvdinfinity.modules.CoroutineDispatcher
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch


class FormViewModel(
        private val interactor: Interactor,
        private val coroutineDispatcher: CoroutineDispatcher,
        app: Application
) : AndroidViewModel(app), LifecycleObserver {

    val maskWatcher = MaskWatcher
    private var locationHashMap = hashMapOf<String, Any>()

    val saveFormState: MutableLiveData<FormViewState<User>> = MutableLiveData()
    val userState: MutableLiveData<FormViewState<User>> = MutableLiveData()

    val estados = ObservableField<Array<String>>().also { it.set(arrayOf()) }
    val cidades = ObservableField<Array<String>>().also { it.set(arrayOf()) }
    val user = MutableLiveData<User>().also { it.value = User() }
    val termsChecked = ObservableField<Boolean>().also { it.set(false) }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        getUser()
        getStatesAndCities()
    }

    fun onSaveFormClick(view: View) {
        if (user.value?.isValid() != true) {
            saveFormState.postValue(FormViewState(FormViewState.Status.ERROR,
                    error = Exception("Preencha os campos obrigatórios")))
            return
        }

        if (termsChecked.get() != true) {
            saveFormState.postValue(FormViewState(FormViewState.Status.ERROR,
                    error = Exception("Necessário aceitar os temos de uso")))
            return
        }

        saveFormState.postValue(FormViewState(FormViewState.Status.LOADING))

        GlobalScope.launch {
            userState.value?.status
            val auxUser = user.value ?: return@launch
            val result = interactor.sendForm(auxUser)

            when (result) {
                is Result.Success -> {
                    saveFormState.postValue(FormViewState(
                            FormViewState.Status.SUCCESS,
                            user.value
                    ))
                    getUser()
                }

                is Result.Error -> {
                    when (result.exception) {
                        is CollisionException -> {
                            saveFormState.postValue(FormViewState(
                                    FormViewState.Status.ERROR,
                                    error = result.exception))
                        }

                        else -> {
                            saveFormState.postValue(FormViewState(
                                    FormViewState.Status.ERROR,
                                    error = Exception("Erro ao realizar esta ação: " +
                                            "${result.exception.message}")))
                        }
                    }

                }
            }

        }

    }

    fun onStateSelected(pos: Int) {
        val selected = estados.get()?.get(pos) ?: return
        val citiesList = locationHashMap[selected] as? List<LinkedTreeMap<String, String>>
        val citiesMapped = citiesList?.map { it["nome"] ?: "" }?.toTypedArray()
        cidades.set(citiesMapped)
    }

    fun onNewToken(pushId: String) {
        user.value?.pushId = pushId
        GlobalScope.launch {
            interactor.sendMessageToken(pushId)
        }
    }

    private fun getStatesAndCities() {
        val text = getApplication<Application>().resources.openRawResource(R.raw.localidades)
                .bufferedReader().use { it.readText() }

        val type = object : TypeToken<HashMap<String, Any>>() {}.type
        locationHashMap = Gson().fromJson(text, type)
        estados.set(locationHashMap.keys.sorted().toTypedArray())
    }


    private fun getUser() {
        GlobalScope.launch {
            saveFormState.postValue(FormViewState(FormViewState.Status.LOADING))
            val result = interactor.getUser()

            when (result) {
                is Result.Success -> {
                    when {
                        result.data.sorteado -> {
                            userState.postValue(FormViewState(FormViewState.Status.DRAWN,
                                    data = result.data))
                        }

                        result.data.isValid() -> {
                            userState.postValue(FormViewState(FormViewState.Status.AWAITING,
                                    data = result.data))
                        }
                    }
                }

                is Result.Error -> {
                    when (result.exception) {
                        is RemoteDataNotFoundException -> userState.postValue(FormViewState(FormViewState.Status.NOT_FOUND))
                        is AuthException -> userState.postValue(FormViewState(FormViewState.Status.UNAUTHENTICATED))
                        else -> userState.postValue(FormViewState(FormViewState.Status.ERROR))
                    }
                }

            }

        }

    }


}

