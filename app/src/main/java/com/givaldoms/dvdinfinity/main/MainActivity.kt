package com.givaldoms.dvdinfinity.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.givaldoms.dvdinfinity.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainBottomNav.setOnNavigationItemSelectedListener { item ->
            onNavDestinationSelected(item, Navigation.findNavController(this, R.id.mobile_navigation))
        }

        val view = findViewById<BottomNavigationView>(R.id.mainBottomNav)
        view.setupWithNavController(Navigation.findNavController(this, R.id.mainContainer))
    }

    override fun onSupportNavigateUp() = findNavController(R.id.mainContainer).navigateUp()

}
