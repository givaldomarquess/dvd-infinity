package com.givaldoms.dvdinfinity.main.form

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.auth.AuthActivity
import com.givaldoms.dvdinfinity.core.extenstions.onItemSelected
import com.givaldoms.dvdinfinity.databinding.FragmentFormBinding
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.content_form.*
import kotlinx.android.synthetic.main.content_form_message.*
import kotlinx.android.synthetic.main.content_form_qrcode.*
import kotlinx.android.synthetic.main.fragment_form.*
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel


class FormFragment : Fragment() {

    private val viewModel: FormViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentFormBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_form,
                container,
                false
        )
        binding.vm = viewModel
        binding.setLifecycleOwner(this)
        lifecycle.addObserver(viewModel)

        observeEvents()

        return binding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        FirebaseMessaging.getInstance().subscribeToTopic("all")
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            viewModel.onNewToken(it.token)
        }
    }

    private fun observeEvents() {
        viewModel.saveFormState.observe(this, Observer {
            progressLayout.visibility = View.GONE

            when (it.status) {
                FormViewState.Status.ERROR -> {
                    Toast.makeText(context, it.error?.message, Toast.LENGTH_SHORT).show()
                }

                FormViewState.Status.SUCCESS -> {
                    Toast.makeText(context, "Informações salvas com sucesso", Toast.LENGTH_SHORT).show()
                }

                FormViewState.Status.LOADING -> progressLayout.visibility = View.VISIBLE


                else -> {
                }
            }
        })

        viewModel.userState.observe(this, Observer {
            qrCodeLayout.visibility = View.GONE
            progressLayout.visibility = View.GONE
            formLayout.visibility = View.GONE
            messageLayout.visibility = View.GONE

            when (it.status) {
                FormViewState.Status.LOADING -> {
                    progressLayout.visibility = View.VISIBLE
                }

                FormViewState.Status.DRAWN -> {
                    qrCodeLayout.visibility = View.VISIBLE
                    fillQrCode(it.data?.id ?: "")
                }

                FormViewState.Status.AWAITING -> {
                    messageLayout.visibility = View.VISIBLE
                    messageTextView.text = "Aguardando resultado do sorteio."
                    //aguardando sorteio
                }

                FormViewState.Status.NOT_FOUND -> {
                    formLayout.visibility = View.VISIBLE
                    //enviar form
                }

                FormViewState.Status.UNAUTHENTICATED -> {
                    context?.startActivity<AuthActivity>()
                    (context as? AppCompatActivity)?.finishAffinity()
                }

                else -> {
                    messageLayout.visibility = View.VISIBLE
                    messageTextView.text = "Erro ao obter informações, verifique sua conexão com a internet e tente novamente."

                }
            }
        })
    }

    override fun onStart() {
        super.onStart()

        estadoSpinner?.onItemSelected {
            viewModel.onStateSelected(it)
        }

        termsTextView.setOnClickListener {
            openUrl("https://firebasestorage.googleapis.com/v0/b/dvd-infinity.appspot.com/o/TERMO%20DE%20USO.pdf?alt=media&token=a3e2b8f6-abf4-4854-8b1f-04e90f40f5d9")
        }
    }

    private fun openUrl(url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun fillQrCode(text: String) {
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 400, 400)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            qrCodeImageView?.imageBitmap = bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

}

