package com.givaldoms.dvdinfinity.main.images

import androidx.lifecycle.*
import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.core.ViewState
import com.givaldoms.dvdinfinity.data.interactor.Interactor
import com.givaldoms.dvdinfinity.model.Midia
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch

class ImagesViewModel(
        private val interactor: Interactor
) : ViewModel(), LifecycleObserver {

    val viewState: MutableLiveData<ViewState<List<Midia>>> = MutableLiveData()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun create() = GlobalScope.launch {
        viewState.postValue(ViewState(ViewState.Status.LOADING))

        val result = interactor.getImages()

        when (result) {
            is Result.Success -> {
                viewState.postValue(ViewState(ViewState.Status.SUCCESS, result.data))
            }

            is Result.Error -> {
                viewState.postValue(ViewState(ViewState.Status.ERROR, error = result.exception))
            }
        }
    }

}
