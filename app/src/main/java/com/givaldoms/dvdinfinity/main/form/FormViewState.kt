package com.givaldoms.dvdinfinity.main.form

class FormViewState<T>(
        val status: Status,
        val data: T? = null,
        val error: Throwable? = null) {

    enum class Status {
        LOADING,
        SUCCESS,
        ERROR,
        UNAUTHENTICATED,
        NOT_FOUND,
        DRAWN,
        AWAITING
    }

}