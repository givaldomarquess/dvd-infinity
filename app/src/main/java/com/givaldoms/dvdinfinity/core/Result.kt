package com.givaldoms.dvdinfinity.core

import com.google.android.gms.tasks.Task
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.Deferred
import retrofit2.HttpException
import java.io.IOException


suspend fun <T> safeApiCall(call: suspend () -> T): Result<T> = try {
    val t = call()
    Result.Success(t)

} catch (httpException: HttpException) {
    when (httpException.code()) {
        409 -> Result.Error(CollisionException())
        else -> Result.Error(RemoteDataNotFoundException(httpException.code()))
    }

} catch (ioException: IOException) {
    Result.Error(ServerException(ioException.message))

} catch (e: Exception) {
    Result.Error(e)
}


sealed class Result<out T> {
    class Success<out T>(val data: T) : Result<T>()
    class Error(val exception: Throwable) : Result<Nothing>()
}


fun <T> Task<T>.asDeferred(): Deferred<T> {
    val deferred = CompletableDeferred<T>()

    deferred.invokeOnCompletion {
        if (deferred.isCancelled) {
            // optional, handle coroutine cancellation however you'd like here
        }
    }

    this.addOnSuccessListener { result -> deferred.complete(result) }
    this.addOnFailureListener { exception -> deferred.completeExceptionally(exception) }

    return deferred
}

open class DataSourceException(message: String? = null, val code: Int? = null) : Exception(message)

class RemoteDataNotFoundException(code: Int? = null) : DataSourceException("Data not found in remote data source", code)

class LocalDataNotFoundException : DataSourceException("Data not found in local data source")

class NoInternetException(message: String? = "No Internet Connection") : Exception(message)

class ServerException(message: String? = "Some server problem") : Exception(message)

class AuthException(message: String = "Usuário não autenticado") : Exception(message)

class CollisionException(message: String = "Usuário já cadastrado. Altere as informações e tente novamente") : Exception(message)