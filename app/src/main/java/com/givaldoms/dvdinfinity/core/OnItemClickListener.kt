package com.givaldoms.dvdinfinity.core

interface OnItemClickListener<in T> {

    fun onItemClick(position: Int, item: T)

}