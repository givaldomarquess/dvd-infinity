package com.givaldoms.dvdinfinity.core.bindingadapters

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.google.android.material.textfield.TextInputLayout


class BindingAdapters {


    companion object {

        @BindingAdapter("app:errorText")
        @JvmStatic
        fun TextInputLayout.errorText(errorMessage: String?) {
            this.error = errorMessage
        }

        @BindingAdapter("app:goneUnless")
        @JvmStatic
        fun View.goneUnless(visible: Boolean) {
            this.visibility = if (visible) View.VISIBLE else View.GONE
        }


        @BindingAdapter(value = ["app:selectedValue", "app:selectedValueAttrChanged"], requireAll = false)
        @JvmStatic
        fun bindSpinnerData(spinner: AppCompatSpinner, newSelectedValue: String?, newTextAttrChanged: InverseBindingListener?) {
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    if (newSelectedValue != null && newSelectedValue == parent.selectedItem) {
                        return
                    }
                    newTextAttrChanged?.onChange()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
            if (newSelectedValue != null) {
                val pos = (spinner.adapter as ArrayAdapter<String>?)?.getPosition(newSelectedValue)
                        ?: return
                spinner.setSelection(pos, true)
            }
        }

        @InverseBindingAdapter(attribute = "app:selectedValue", event = "app:selectedValueAttrChanged")
        @JvmStatic
        fun captureSelectedValue(spinner: AppCompatSpinner): String {
            return spinner.selectedItem as String? ?: ""
        }

    }

}