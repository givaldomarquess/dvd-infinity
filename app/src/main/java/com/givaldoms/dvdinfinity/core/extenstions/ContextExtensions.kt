@file:Suppress("UNUSED_PARAMETER")

package com.givaldoms.dvdinfinity.core.extenstions

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import java.io.*
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by givaldoms on 22/06/2018.
 */

const val REQUEST_CAPTURE_IMAGE = 0xFCC
private var imageFilePath = ""

fun Context.shareUri(message: String, image: Uri? = Uri.EMPTY): Boolean {
    if (image == null) return false

    val intent = Intent(Intent.ACTION_SEND)
    if (image == Uri.EMPTY) {
        intent.type = "text/plain"
    } else {
        intent.type = "image/jpeg"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.putExtra(Intent.EXTRA_STREAM, image)
    }

    intent.putExtra(Intent.EXTRA_TEXT, message)
    this.startActivity(intent)
    return true
}

fun Context.shareUri(message: String, bitmap: Bitmap) =
        shareUri(message, getImageUri(bitmap))

fun Context.shareWhatsApp(obj: Any) = shareUri(obj.toString())


fun Context.openInMaps(lat: Float, long: Float, locationName: String = "Aqui") {
    val gmmIntentUri = Uri.parse("geo:$lat,$long?q=$lat,$long($locationName)")
    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
    mapIntent.setPackage("com.google.android.apps.maps")
    startActivity(mapIntent)
}

fun AppCompatActivity.openCameraIntent() {
    val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    if (pictureIntent.resolveActivity(packageManager) != null) {
        var photoFile: File? = null

        try {
            photoFile = createImageFile()
        } catch (ex: IOException) {

        }
        if (photoFile != null) {
            val photoURI = FileProvider.getUriForFile(this,
                    "$packageName.provider", photoFile)
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    photoURI)
            this.startActivityForResult(pictureIntent,
                    REQUEST_CAPTURE_IMAGE)
        }
    }
}

fun AppCompatActivity.cameraResultHandler(requestCode: Int, resultCode: Int, quality: Int = 50): String? {

    if (requestCode != REQUEST_CAPTURE_IMAGE || resultCode != RESULT_OK) return null

    val photo = BitmapFactory.decodeFile(imageFilePath)
    val fileOut = FileOutputStream(imageFilePath)

    photo.compress(Bitmap.CompressFormat.JPEG, quality, fileOut)
    fileOut.close()

    return imageFilePath

}

@Throws(IOException::class)
fun Context.createImageFile(): File {
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
            Locale.getDefault()).format(Date())
    val imageFileName = "IMG_" + timeStamp + "_"
    val storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    val image = File.createTempFile(imageFileName, ".jpg", storageDir)

    imageFilePath = image.absolutePath
    return image
}


fun Context.loadRaw(resId: Int) =
        this.resources.openRawResource(resId).readTextAndClose()

fun InputStream.readTextAndClose(charset: Charset = Charsets.UTF_8): String {
    return this.bufferedReader(charset).use { it.readText() }
}

fun <T : View?> LinearLayoutManager.findView(position: Int, id: Int): T? {
    return this.findViewByPosition(position)?.findViewById<T>(id)
}

fun Context.getImageUri(bitmap: Bitmap, imageName: String = "imagem.jpg"): Uri? = try {
    val bytes = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(contentResolver, bitmap, imageName, null)
    Uri.parse(path)
} catch (e: Exception) {
    null
}


fun Context.isAppInstalled(packageName: String) = try {
    packageManager.getApplicationInfo(packageName, 0)
    true
} catch (e: PackageManager.NameNotFoundException) {
    false
}
