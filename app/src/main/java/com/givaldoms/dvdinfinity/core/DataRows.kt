package com.givaldoms.dvdinfinity.core

class DataRows<T>(val data: Content<T>) {

    class Content<T>(
            var count: Int,
            var rows: List<T>
    )
}

class Data<T>(val data: List<T>)
