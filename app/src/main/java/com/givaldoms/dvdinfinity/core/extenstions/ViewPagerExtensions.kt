package com.givaldoms.dvdinfinity.core.extenstions

import androidx.viewpager.widget.ViewPager

/**
 * Created by givaldoms on 21/06/2018.
 */

fun ViewPager.OnPageSelectedListener(pageSelected: (Int) -> Unit) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(position: Int, positionOffset: Float,
                                    positionOffsetPixels: Int) {}

        override fun onPageSelected(position: Int) {
           pageSelected(position)
        }
    })
}