package com.givaldoms.dvdinfinity.core.extenstions

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by givaldoms on 22/06/2018.
 */


fun Long.timestampToDate() = try {

    val aux = SimpleDateFormat("dd/MM/yyyy", Locale.US)
            .format(this)
            .split("/")
    "${aux[0]} de ${aux[1].toInt().minus(1).toMonthOfYear()} de ${aux[2]}"

} catch (ex: Exception) {
    ""
}

fun Long.timestampToDateTime(): String =
        SimpleDateFormat("dd/MM/yyyy 'às' HH:mm:ss", Locale.US)
                .format(this)


fun SimpleDateFormat.toMilli(date: String) = this.parse(date).time

fun Int.toMonthOfYear() = when (this) {
    0 -> "Janeiro"
    1 -> "Fevereiro"
    2 -> "Março"
    3 -> "Abril"
    4 -> "Maio"
    5 -> "Junho"
    6 -> "Julho"
    7 -> "Agosto"
    8 -> "Setembro"
    9 -> "Outubro"
    10 -> "Novembro"
    11 -> "Dezembro"
    12 -> "Agosto"
    else -> ""
}

