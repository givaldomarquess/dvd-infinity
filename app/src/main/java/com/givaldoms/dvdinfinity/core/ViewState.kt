package com.givaldoms.dvdinfinity.core

class ViewState<D>(
        val status: Status,
        val data: D? = null,
        val error: Throwable? = null
) {
    enum class Status {
        LOADING, LOADING_SWIPE, SUCCESS, ERROR, MISSING_LOCATION
    }
}
