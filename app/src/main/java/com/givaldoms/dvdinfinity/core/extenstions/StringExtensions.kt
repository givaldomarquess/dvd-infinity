package com.givaldoms.dvdinfinity.core.extenstions

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by givaldoms on 28/05/2018.
 */

fun String.isEmail(): Boolean {
    val regex = "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
    return pattern.matcher(this).find()
}

fun String.isNotEmail() = !this.isEmail()


fun String.utcToData(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    val date = sdf.parse(this)

    val aux = SimpleDateFormat("dd/MM/yyyy", Locale.US)
            .format(date)
            .split("/")
    return "${aux[0]} de ${aux[1].toInt().minus(1).toMonthOfYear()} de ${aux[2]}"
}

fun String.formatToDate(): String {
    val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    val date = sdf.parse(this)

    return SimpleDateFormat("yyyy-MM-dd", Locale.US)
            .format(date)
}

fun String.utcToDataHora(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone("UTC-3")

    val date = sdf.parse(this)
    val sdf2 = SimpleDateFormat("dd/MM/yyyy 'às' HH:mm:ss", Locale.getDefault())
    sdf2.timeZone = TimeZone.getTimeZone("America/Maceio")
    return sdf2.format(date)

}
