package com.givaldoms.dvdinfinity.core.extenstions

import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.EditText

/**
 * Created by givaldoms on 28/05/2018.
 */

fun EditText.putText(text: Any) {
    this.text = Editable.Factory.getInstance().newEditable(text.toString())
}

fun EditText.onTextChanged(f: (String) -> Any?) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            f(s?.toString() ?: "")
        }
    })
}

fun EditText.onActionDone(f: (String) -> Any) {
    this.setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            f(this.text.toString())
        }
        false
    }

}

fun EditText.increaseOne() {
    if (this.text.isBlank()) {
        this.putText("1")

    } else {
        val text = this.text.toString()
        val t = text.toIntOrNull() ?: return
        this.putText((t + 1))
    }

    moveCursorToEnd()
}

fun EditText.decreaseOne() {
    val t = this.text.toString().toIntOrNull() ?: return

    if (t < 1) return
    this.putText((t - 1))
    moveCursorToEnd()

}

fun EditText.moveCursorToEnd() {
    this.setSelection(this.text.length)
}