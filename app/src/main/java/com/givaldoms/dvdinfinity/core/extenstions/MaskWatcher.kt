package com.givaldoms.dvdinfinity.core.extenstions

/*
MIT License
Copyright (c) 2016 Diego Yasuhiko Kurisaki
*/

/* Example:
  mEmailView.addTextChangedListener(new MaskWatcher("###-##"));
*/

import android.text.Editable
import android.text.TextWatcher

class MaskWatcher(private val mask: String) : TextWatcher {
    private var isRunning = false
    private var isDeleting = false

    override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {
        isDeleting = count > after
    }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        if (isRunning || isDeleting) {
            return
        }
        isRunning = true

        val editableLength = editable.length
        if (editableLength < mask.length) {
            when {
                mask.contains(editable.last()) && mask[editableLength - 1] != editable.last() -> editable.delete(editableLength - 1, editableLength)
                mask.contains(editable.last()) && mask[editableLength - 1] == editable.last() -> {/*JUST FILLING*/
                }
                mask[editableLength] != '#' -> editable.append(mask[editableLength])
                mask[editableLength - 1] != '#' -> editable.insert(editableLength - 1, mask, editableLength - 1, editableLength)
            }
        }

        isRunning = false
    }

    companion object {
        fun buildCpf() = MaskWatcher("###.###.###-##")
        fun buildDate() = MaskWatcher("##/##/####")
        fun buildCep() = MaskWatcher("#####-###")
        fun buildPhoneNumber() = MaskWatcher("(##) # ####-####")
        fun buildCreditCardValidThruDate() = MaskWatcher("##/##")
    }
}