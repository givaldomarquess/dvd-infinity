package com.givaldoms.dvdinfinity.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.core.ViewState
import com.givaldoms.dvdinfinity.databinding.FragmentLoginBinding
import com.givaldoms.dvdinfinity.main.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentLoginBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_login,
                container,
                false)

        binding.vm = viewModel
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer {
            loginProgressBar.visibility = View.GONE
            loginButton.isEnabled = true
            when (it.status) {
                ViewState.Status.SUCCESS -> {
                    activity?.run {
                        startActivity<MainActivity>()
                        finishAffinity()
                    }
                }

                ViewState.Status.LOADING -> {
                    loginProgressBar.visibility = View.VISIBLE
                    loginButton.isEnabled = false
                }
                else -> {
                }
            }
        })

        viewModel.recoverPasswordState.observe(this, Observer {
            if (it.status == ViewState.Status.SUCCESS) {
                Toast.makeText(context, it.data, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        (activity as AppCompatActivity).startActivity<MainActivity>()

        loginSignUpButton?.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.signUpFragment)
        }


    }

}
