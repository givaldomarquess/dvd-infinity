package com.givaldoms.dvdinfinity.auth.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.givaldoms.dvdinfinity.R
import com.givaldoms.dvdinfinity.core.ViewState
import com.givaldoms.dvdinfinity.databinding.FragmentSignUpBinding
import com.givaldoms.dvdinfinity.main.MainActivity
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import kotlinx.android.synthetic.main.fragment_sign_up.*
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SignUpFragment : Fragment() {

    companion object {
        fun newInstance() = SignUpFragment()
    }

    private val viewModel: SignUpViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding: FragmentSignUpBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_sign_up,
                container,
                false
        )
        binding.vm = viewModel


//        (activity as AppCompatActivity).run {
//            setSupportActionBar(signUpToolbar)
//            supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        }
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer {
            signUpProgressBar.visibility = View.GONE
            signUpButton.isEnabled = true

            when (it.status) {
                ViewState.Status.SUCCESS -> {
                    activity?.run {
                        startActivity<MainActivity>()
                        finishAffinity()
                    }
                }
                ViewState.Status.ERROR -> {
                    if (it.error is FirebaseAuthUserCollisionException) {
                        Toast.makeText(activity, "E-mail já em uso", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(
                                activity,
                                "Erro desconhecido, tente novamente mais tarde",
                                Toast.LENGTH_LONG
                        ).show()
                    }
                }

                ViewState.Status.LOADING -> {
                    signUpProgressBar.visibility = View.VISIBLE
                    signUpButton.isEnabled = false
                }

                else -> {
                }

            }
        })
    }

}
