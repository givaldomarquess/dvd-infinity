package com.givaldoms.dvdinfinity.auth.login

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.givaldoms.dvdinfinity.core.Result
import com.givaldoms.dvdinfinity.core.ViewState
import com.givaldoms.dvdinfinity.core.extenstions.isNotEmail
import com.givaldoms.dvdinfinity.data.interactor.Interactor
import com.givaldoms.dvdinfinity.modules.CoroutineDispatcher
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch
import timber.log.Timber


class LoginViewModel(
        private val interactor: Interactor,
        private val coroutineDispatcher: CoroutineDispatcher
) : ViewModel() {

    val viewState: MutableLiveData<ViewState<Boolean>> = MutableLiveData()
    val recoverPasswordState: MutableLiveData<ViewState<String>> = MutableLiveData()

    val email = ObservableField<String>()
    val password = ObservableField<String>()
    val emailError = ObservableField<String?>()
    val passwordError = ObservableField<String?>()


    fun onLoginClick(view: View) {
        GlobalScope.launch {
            val auxEmail = email.get() ?: ""
            val auxPassword = password.get() ?: ""

            if (!validForm(auxEmail, auxPassword)) return@launch
            viewState.postValue(ViewState(ViewState.Status.LOADING))

            val loginResult = interactor.login(auxEmail, auxPassword)

            GlobalScope.launch(coroutineDispatcher.uiContext) {
                when (loginResult) {
                    is Result.Success -> {
                        viewState.postValue(ViewState(ViewState.Status.SUCCESS))
                    }
                    is Result.Error -> {
                        viewState.postValue(ViewState(ViewState.Status.ERROR, error = loginResult.exception))
                        errorHandler(loginResult.exception)
                    }
                }
            }

        }
    }

    private fun validForm(email: String, password: String): Boolean {
        emailError.set(null)
        passwordError.set(null)

        var isValid = false
        val auxEmail = email.toLowerCase().trim()
        val auxPassword = password.trim()

        when {
            auxEmail.isBlank() -> emailError.set("Campo obrigatório")
            auxEmail.isNotEmail() -> emailError.set("Email inválido")
            else -> {
                emailError.set(null)
                isValid = true
            }
        }

        when {
            auxPassword.isBlank() -> passwordError.set("Campo obrigatório")
            auxPassword.length < 6 -> passwordError.set("Senha inválida")
            else -> {
                passwordError.set(null)
                isValid = isValid && true
            }
        }

        return isValid
    }

    fun onRecoverClick(view: View) {

        GlobalScope.launch {
            val auxEmail = email.get()?.toLowerCase()?.trim() ?: ""
            if (!validForm(auxEmail, "123456789")) return@launch

            recoverPasswordState.postValue(ViewState(ViewState.Status.LOADING))

            GlobalScope.launch(coroutineDispatcher.uiContext) {
                interactor.recoverPassword(auxEmail)
                recoverPasswordState.postValue(ViewState(ViewState.Status.SUCCESS,
                        "E-mail enviado com sucesso"))

            }
        }
    }

    private fun errorHandler(exception: Throwable?) {
        Timber.e(exception)

        when (exception) {
            is FirebaseAuthInvalidUserException -> emailError.set("Email não cadastrado")
            is FirebaseAuthInvalidCredentialsException -> passwordError.set("Senha inválida")
        }
    }

}
