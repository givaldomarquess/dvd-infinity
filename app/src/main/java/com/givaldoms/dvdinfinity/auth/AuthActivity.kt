package com.givaldoms.dvdinfinity.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.givaldoms.dvdinfinity.R
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        setupNavigation()
    }

    private fun setupNavigation() {
        setSupportActionBar(mainToolbar)
        val navController = findNavController(R.id.authContainer)
        setupActionBarWithNavController(navController)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.authContainer).navigateUp()

}
