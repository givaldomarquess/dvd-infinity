package com.givaldoms.dvdinfinity.modules

import com.givaldoms.dvdinfinity.BuildConfig
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.android.Main
import kotlin.coroutines.experimental.CoroutineContext

object CoroutineDispatcherFactory {
    fun provideCoroutineDispatcher(): CoroutineDispatcher {
        return when {
            BuildConfig.COROUTINES_MODE -> CoroutineDispatcher(Dispatchers.Main, Dispatchers.Default)
            else -> CoroutineDispatcher(Dispatchers.Unconfined, Dispatchers.Unconfined)
        }
    }
}

data class CoroutineDispatcher(val uiContext: CoroutineContext, val bgContext: CoroutineContext)