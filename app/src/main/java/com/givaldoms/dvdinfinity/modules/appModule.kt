package com.givaldoms.dvdinfinity.modules

import com.givaldoms.dvdinfinity.auth.login.LoginViewModel
import com.givaldoms.dvdinfinity.auth.signup.SignUpViewModel
import com.givaldoms.dvdinfinity.data.interactor.Interactor
import com.givaldoms.dvdinfinity.data.interactor.InteractorImpl
import com.givaldoms.dvdinfinity.main.form.FormViewModel
import com.givaldoms.dvdinfinity.main.images.ImagesViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    single { InteractorImpl(get(), get()) as Interactor }

    viewModel { LoginViewModel(get(), get()) }
    viewModel { SignUpViewModel(get(), get()) }
    viewModel { FormViewModel(get(), get(), get()) }
    viewModel { ImagesViewModel(get()) }

}
