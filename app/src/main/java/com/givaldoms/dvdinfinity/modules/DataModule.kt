package com.givaldoms.dvdinfinity.modules

import com.givaldoms.dvdinfinity.data.repository.*
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.defaultSharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val dataModule = module {

    single { createOkHttpClient() }
    single { createWebService(get(), DatasourceProperties.SERVER_URL) as WebService }
    single { FirebaseAuth.getInstance()}

    single { RemoteRepositoryImpl(get()) as RemoteRepository }
    single { FirebaseDataSourceImpl(get()) as FirebaseDataSource}

    single { androidContext().defaultSharedPreferences }

    single { CoroutineDispatcherFactory.provideCoroutineDispatcher() }

}