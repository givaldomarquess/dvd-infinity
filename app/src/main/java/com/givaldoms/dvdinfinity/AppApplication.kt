package com.givaldoms.dvdinfinity

import android.app.Application
import com.givaldoms.dvdinfinity.modules.appModule
import com.givaldoms.dvdinfinity.modules.dataModule
import com.google.firebase.FirebaseApp
import org.koin.android.ext.android.startKoin


class AppApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        startKoin(this, listOf(appModule, dataModule))
    }

}