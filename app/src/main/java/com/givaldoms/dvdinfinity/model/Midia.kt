package com.givaldoms.dvdinfinity.model

data class Midia(
    val id : String,
    val url: String,
    val mensagem: String
)