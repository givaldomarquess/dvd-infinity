package com.givaldoms.dvdinfinity.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.givaldoms.dvdinfinity.BR

data class User(
        private var _id: String = "",
        private var _nome: String = "",
        private var _rg: String = "",
        private var _orgEmissor: String = "",
        private var _whatsapp: String = "",
        private var _instagram: String = "",
        private var _sexo: String = "",
        private var _dataNascimento: String = "",
        private var _cidade: String = "",
        private var _bairro: String = "",
        private var _pushId: String = "",
        private var _indicacao: String = "",
        private var _sorteado: Boolean = false
) : BaseObservable() {

    var id: String
        @Bindable get() = _id
        set(value) {
            _id = value
            notifyPropertyChanged(BR.id)
        }

    var nome: String
        @Bindable get() = _nome
        set(value) {
            _nome = value
            notifyPropertyChanged(BR.nome)
        }
    var rg: String
        @Bindable get() = _rg
        set(value) {
            _rg = value
            notifyPropertyChanged(BR.rg)
        }

    var orgEmissor: String
        @Bindable get() = _orgEmissor
        set(value) {
            _orgEmissor = value
            notifyPropertyChanged(BR.orgEmissor)
        }

    var whatsapp: String
        @Bindable get() = _whatsapp
        set(value) {
            _whatsapp = value
            notifyPropertyChanged(BR.whatsapp)
        }

    var instagram: String
        @Bindable get() = _instagram
        set(value) {
            _instagram = value
            notifyPropertyChanged(BR.instagram)
        }

    var sexo: String
        @Bindable get() = _sexo
        set(value) {
            _sexo = value
            notifyPropertyChanged(BR.sexo)
        }

    var dataNascimento: String
        @Bindable get() = _dataNascimento
        set(value) {
            _dataNascimento = value
            notifyPropertyChanged(BR.dataNascimento)
        }

    var cidade: String
        @Bindable get() = _cidade
        set(value) {
            _cidade = value
            notifyPropertyChanged(BR.cidade)
        }

    var bairro: String
        @Bindable get() = _bairro
        set(value) {
            _bairro = value
            notifyPropertyChanged(BR.bairro)
        }

    var pushId: String
        @Bindable get() = _pushId
        set(value) {
            _pushId = value
            notifyPropertyChanged(BR.pushId)
        }

    var indicacao: String
        @Bindable get() = _indicacao
        set(value) {
            _indicacao = value
            notifyPropertyChanged(BR.indicacao)
        }

    var sorteado: Boolean
        @Bindable get() = _sorteado
        set(value) {
            _sorteado = value
            notifyPropertyChanged(BR.sorteado)
        }

    fun isValid() =
            nome.isNotBlank() &&
                    rg.isNotBlank() &&
                    orgEmissor.isNotBlank() &&
                    whatsapp.isNotBlank() &&
                    instagram.isNotBlank() &&
                    sexo.isNotBlank() &&
                    dataNascimento.isNotBlank() &&
                    cidade.isNotBlank() &&
                    pushId.isNotBlank()

}