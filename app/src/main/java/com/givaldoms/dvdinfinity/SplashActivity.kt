package com.givaldoms.dvdinfinity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.givaldoms.dvdinfinity.main.MainActivity
import org.jetbrains.anko.startActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity<MainActivity>()
        finish()
    }
}
